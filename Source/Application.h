#ifndef _H_APPLICATION_
#define _H_APPLICATION_

bool Initialize(int w, int h);
bool Update(float dt);
void Render();
void Shutdown();
void Resize(int w, int h);

#endif
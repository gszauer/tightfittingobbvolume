#ifndef _H_OBJLOADER_
#define _H_OBJLOADER_

#include <vector>
#include "glm/glm.hpp"
#include "glm/ext.hpp"

struct Face {
    unsigned int a;
    unsigned int b;
    unsigned int c;
};

class Obj {
protected:
    std::vector<Face> Faces;
    std::vector<glm::vec3> Positions;
    std::vector<glm::vec3> Normals;
    std::vector<glm::vec2> TexCoords;
    glm::mat4 Transform;
    bool error;
    
    unsigned int VBO;
    unsigned int IBO;
protected:
    Obj();
    Obj(const Obj&);
    Obj& operator=(const Obj&);
public:
    Obj(const char* filename);
    ~Obj();
    
    int GetIndexCount();
    int GetVertCount();

    const unsigned int* GetFaces();
    const float* GetPositions();
    const float* GetNormals();
    const float* GetTexCoords();
    
    const Face* GetFace(int index);
    int GetNumFaces();
    const glm::vec3* GetPosition(int index);
    
    void Render(int position);
    void Render(int position, int normal);
    void RenderFixedFunction();
    
    glm::mat4 GetTransform();
    void SetTransform(const glm::mat4& t);
    bool GetError();
};

#endif // OBJLOADER_H
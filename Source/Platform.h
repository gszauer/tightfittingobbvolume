#ifndef _H_PLATFORM_
#define _H_PLATFORM_

#if __APPLE__
#include <OpenGL/gl.h>
#include <GLUT/glut.h>
#elif __linux
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glut.h>
#elif _WIN32
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include "GL/openglut.h"
#else
#error Include OpenGL Headers
#endif

#if __linux | __APPLE__
#include <sys/time.h>
#endif

#endif
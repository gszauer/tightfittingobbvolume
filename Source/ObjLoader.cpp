#include "ObjLoader.h"
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cstring>
#include <map>
#include "Platform.h"

struct FaceVert {
    FaceVert() : vert(-1), norm(-1), coord(-1) {}

    int vert;
    int norm;
    int coord;
};

struct vert_less { // Handle any size mesh
    bool operator() (const FaceVert& lhs, const FaceVert& rhs) const {
        if (lhs.vert != rhs.vert) return (lhs.vert<rhs.vert);
        if (lhs.norm != rhs.norm) return (lhs.norm<rhs.norm);
        if (lhs.coord!=rhs.coord) return (lhs.coord<rhs.coord);
        return false;
    }
};

Obj::~Obj() {
    if (VBO != 0)
        glDeleteBuffers(1, &VBO);
    if (IBO != 0)
        glDeleteBuffers(1, &IBO);
}

Obj::Obj(const char* filename) {
    VBO = 0;
    IBO = 0;
    error = false;
    
    std::ifstream inf;
    inf.open(filename, std::ios_base::in);
    if (!inf.is_open()) {
        error  = true;
        std::cout << "[!] Failed to load file: " << filename << '\n';
        return;
    }

    Positions.clear();
    Normals.clear();
    TexCoords.clear();
    Faces.clear();

    char *delims = (char*)" \n\r";
    const unsigned int CHARACTER_COUNT = 500;
    char line[CHARACTER_COUNT] = {0};

    std::vector<glm::vec3> verts;
    std::vector<glm::vec3> norms;
    std::vector<glm::vec2> texcoords;

    std::map<FaceVert, int, vert_less> uniqueverts;
    unsigned int vert_count = 0;

    while (inf.good()) {
        memset( (void*)line, 0, CHARACTER_COUNT);
        inf.getline(line, CHARACTER_COUNT);
        if (inf.eof()) break;

        char *token = strtok(line, delims);
        if (token == NULL || token[0] == '#' || token[0] == '$')
            continue;

        // verts look like:
        //	v float float float
        if (strcmp(token, "v") == 0) {
            float x=0, y=0, z=0, w=1;
            sscanf(line+2, "%f %f %f %f", &x, &y, &z, &w);
            verts.push_back( glm::vec3(x/w,y/w,z/w) );
        }
        // normals:
        // 	nv float float float
        else if (strcmp(token, "vn") == 0) {
            float x=0, y=0, z=0;
            sscanf(line+3, "%f %f %f", &x, &y, &z);
            norms.push_back( glm::vec3(x,y,z) );
        }
        // texcoords:
        //	vt	float float
        else if (strcmp(token, "vt") == 0) {
            float x=0, y=0, z=0;
            sscanf(line+3, "%f %f %f", &x, &y, &z);
            texcoords.push_back( glm::vec2(x, y) );
        }

        // keep track of smoothing groups
        // s [number|off]
        else if (strcmp(token, "s") == 0) {

        }

        // faces start with:
        //	f
        else if (strcmp(token, "f") == 0) {

            std::vector<int> vindices;
            std::vector<int> nindices;
            std::vector<int> tindices;

            // fill out a triangle from the line, it could have 3 or 4 edges
            char *lineptr = line + 2;
            while (lineptr[0] != 0) {
                while (lineptr[0] == ' ') ++lineptr;

                int vi=0, ni=0, ti=0;
                if (sscanf(lineptr, "%d/%d/%d", &vi, &ni, &ti) == 3) {
                    vindices.push_back(vi-1);
                    nindices.push_back(ni-1);
                    tindices.push_back(ti-1);
                }
                else
                if (sscanf(lineptr, "%d//%d", &vi, &ni) == 2) {
                    vindices.push_back(vi-1);
                    nindices.push_back(ni-1);
                }
                else
                if (sscanf(lineptr, "%d/%d", &vi, &ti) == 2) {
                    vindices.push_back(vi-1);
                    tindices.push_back(ti-1);
                }
                else
                if (sscanf(lineptr, "%d", &vi) == 1) {
                    vindices.push_back(vi-1);
                }

                while(lineptr[0] != ' ' && lineptr[0] != 0) ++lineptr;
            }

            // being that some exporters can export either 3 or 4 sided polygon's
            // convert what ever was exported into triangles
            for (size_t i=1; i<vindices.size()-1; ++i) {
                Face face;
                FaceVert tri;

                tri.vert = vindices[0];
                if (!nindices.empty())
                    tri.norm = nindices[0];
                if (!tindices.empty())
                    tri.norm = tindices[0];

                if (uniqueverts.count(tri) == 0)
                    uniqueverts[tri] = vert_count++;
                face.a = uniqueverts[tri];

                tri.vert = vindices[i];
                if (!nindices.empty())
                    tri.norm = nindices[i];
                if (!tindices.empty())
                    tri.norm = tindices[i];

                if (uniqueverts.count(tri) == 0)
                    uniqueverts[tri] = vert_count++;
                face.b = uniqueverts[tri];

                tri.vert = vindices[i+1];
                if (!nindices.empty())
                    tri.norm = nindices[i+1];
                if (!tindices.empty())
                    tri.norm = tindices[i+1];

                if (uniqueverts.count(tri) == 0)
                    uniqueverts[tri] = vert_count++;
                face.c = uniqueverts[tri];
                Faces.push_back(face);
            }
        }
    }
    inf.close();

    // use resize instead of reserve because we'll be indexing in random locations.
    Positions.resize(vert_count);
    if (norms.size() > 0)
        Normals.resize(vert_count);
    if (texcoords.size() > 0)
        TexCoords.resize(vert_count);

    std::map<FaceVert, int, vert_less>::iterator iter;
    for (iter = uniqueverts.begin(); iter != uniqueverts.end(); ++iter) {

        Positions[iter->second] = verts[iter->first.vert];

        if ( norms.size() > 0 ) {
            Normals[iter->second] = norms[iter->first.norm];
        }

        if ( texcoords.size() > 0) {
            TexCoords[iter->second] = texcoords[iter->first.coord];
        }
    }
    
    std::vector<glm::vec3> vertexList;
    //std::cout << "Size: " << vertexList.size() << "\n";
    vertexList.reserve(Positions.size() + Normals.size());
    //std::cout << "Size: " << vertexList.size() << "\n";
    vertexList.insert(vertexList.end(), Positions.begin(), Positions.end());
    //std::cout << "Size: " << vertexList.size() << "\n";
    vertexList.insert(vertexList.end(), Normals.begin(), Normals.end());
    //std::cout << "Size: " << vertexList.size() << "\n";
    
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * (vertexList.size()), &vertexList[0].x, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &IBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Face) * Faces.size(), &Faces[0].a, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Obj::Render(int position, int normal) {
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
    
    glEnableVertexAttribArray(position);
    glEnableVertexAttribArray(normal);
    
    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glVertexAttribPointer(normal, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(Positions.size() * sizeof(glm::vec3)));
    
    glDrawElements(GL_TRIANGLES, int(Faces.size() * 3), GL_UNSIGNED_INT, 0);
    
    glDisableVertexAttribArray(position);
    glDisableVertexAttribArray(normal);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Obj::Render(int position) {
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
    
    glEnableVertexAttribArray(position);
    
    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    glDrawElements(GL_TRIANGLES, int(Faces.size() * 3), GL_UNSIGNED_INT, 0);
    
    glDisableVertexAttribArray(position);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

glm::mat4 Obj::GetTransform() {
    return Transform;
}

void Obj::SetTransform(const glm::mat4& t) {
    Transform = t;
}

bool Obj::GetError() {
    return error;
}

void Obj::RenderFixedFunction() {
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, GetPositions());
    glNormalPointer(GL_FLOAT, 0, GetNormals());
    glDrawElements(GL_TRIANGLES, GetNumFaces() * 3, GL_UNSIGNED_INT, GetFaces());
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
}

int Obj::GetIndexCount() {
    return (int)Faces.size() * 3;
}

int Obj::GetVertCount() {
    return (int)Positions.size();
}

const unsigned int* Obj::GetFaces() {
    return (const unsigned int*)&Faces[0];
}

const Face* Obj::GetFace(int index) {
    return &(Faces[index]);
}

const glm::vec3* Obj::GetPosition(int index) {
    return &(Positions[index]);
}

int Obj::GetNumFaces() {
    return int(Faces.size());
}

const float* Obj::GetPositions() {
    //return (const float*)&Positions[0].x;
    return glm::value_ptr(Positions[0]);
}

const float* Obj::GetNormals() {
    return (const float*)&Normals[0];
}

const float* Obj::GetTexCoords() {
    return (const float*)&TexCoords[0];
}
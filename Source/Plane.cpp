#include "Plane.h"

// Create plane from point normal
// From http://www.blitzbasic.com/Community/posts.php?topic=42657
// http://graphics.stanford.edu/~mdfisher/Code/Engine/Plane.cpp.html
Plane::Plane(const glm::vec3& point, const glm::vec3& norm) {
    normal = glm::normalize(norm);
    distance = -glm::dot(normal, point);
}

std::vector<glm::vec3> Plane::ProjectPoints(const std::vector<glm::vec3>& points) {
    std::vector<glm::vec3> result;
    result.reserve(points.size());
    for (int i = 0, isize = int(points.size()); i < isize; ++i)
        result.push_back(ProjectPoint(points[i]));
    return result;
}

// http://www.gamedev.net/topic/395194-closest-point-on-plane--distance/
glm::vec3 Plane::ProjectPoint(const glm::vec3& point) {
    glm::vec3 pointOnPlane = (-normal) * distance;
    float dist = glm::dot(normal, point - pointOnPlane);
    return point - dist * normal;
}
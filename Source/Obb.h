#ifndef _H_OBB_
#define _H_OBB_

#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include <vector>

static glm::vec3 obbDbg1;
static glm::vec3 obbDbg2;

struct OBB {
    glm::vec3 xAxis; // Right
    glm::vec3 yAxis; // Up
    glm::vec3 zAxis; // Forward
    glm::vec3 position;
    glm::vec3 extents;
    
    OBB(glm::vec3 lightPosition, glm::vec3 lightNormal, std::vector<glm::vec3> points);
    void RenderFixedFunction();
    void ToFrusutm(glm::mat4& outModelView, glm::mat4& outProjection);
};

#endif
#include "Application.h"
#include "Platform.h"

#include <iostream>
#include <cmath>
#include <vector>
#include <map>
#include <iostream>
#include "glm/glm.hpp"
#include "glm/ext.hpp"

#define WINFOW_TITLE    "Cascade Test"
#define WINDOW_WIDTH    800
#define WINDOW_HEIGHT   600
#define UPDATE_TIME     17 /* ((1 / 60) * 1000) rounded up */

void display();
void updateTimer(int windowId);
void reshape(int width, int height);
void keyDown(unsigned char c, int x, int y);
double GetMilliseconds();
double lastTime;

float updateSpeed = 1.0f;
int step = 0;
bool exitApp = false;

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowPosition(0,0);
    glutInitWindowSize(WINDOW_WIDTH , WINDOW_HEIGHT);
    glutCreateWindow(WINFOW_TITLE);
    
#if !__APPLE__
    GLenum glewError = glewInit();
    if(glewError != GLEW_OK) {
        std::cout << "Error! Could not load OpenGL extensions!\n";
        std::cout << "\t" << glewGetErrorString(glewError) << "\n";
        return 0;
    }
#endif
    
    glClearColor(0.5f, 0.6f, 0.7f, 1.0f);
    
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    
    if (!Initialize(WINDOW_WIDTH, WINDOW_HEIGHT))
        exit(0);
    
    reshape(WINDOW_WIDTH, WINDOW_HEIGHT);
    atexit(Shutdown);
    
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyDown);
    glutIgnoreKeyRepeat(false); // Key is continually held down
    glutTimerFunc(UPDATE_TIME, updateTimer, glutGetWindow());
    
    lastTime = GetMilliseconds();
    glutMainLoop();
    
    return 0;
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    Render();
    glutSwapBuffers();
}

void updateTimer(int windowId) {
    double time = GetMilliseconds();
    float deltaTime = float(time - lastTime) * 0.001f;
    lastTime = time;
    
    if (step != 0) {
        if (step == -1) updateSpeed = -1.0f;
        else if (step == 1) updateSpeed = 1.0f;
    }
    
    if (!Update(deltaTime) || exitApp) {
        Shutdown();
        exit(0);
    }
    
    if (step != 0) {
        updateSpeed = 0.0f;
        step = 0.0f;
    }
    
    glutTimerFunc(UPDATE_TIME, updateTimer, windowId);
    glutPostRedisplay();
}

void reshape(int width, int height) {
    glViewport(0, 0, width, height);
    Resize(width, height);
}

void keyDown(unsigned char c, int x, int y) {
    //std::cout << "Key: " << c << "\n";
    
    if (c == '1' || c == ' ') {
        if (updateSpeed > 0.0f)
            updateSpeed = 0.0f;
        else
            updateSpeed = 1.0f;
    }
    else if (c == '2' || c == 'z') {
        updateSpeed = 0.0f;
        step = -1;
    }
    else if (c == '3' || c == 'x') {
        updateSpeed = 0.0f;
        step = 1;
    }
    else if (c == '') {
        exitApp = true;
    }
}

double GetMilliseconds() {
#if __linux | __APPLE__
    static timeval s_tTimeVal;
    gettimeofday(&s_tTimeVal, NULL);
    double time = s_tTimeVal.tv_sec * 1000.0; // sec to ms
    time += s_tTimeVal.tv_usec / 1000.0; // us to ms
    return time;
#elif _WIN32
    static LARGE_INTEGER s_frequency;
    static BOOL s_use_qpc = QueryPerformanceFrequency(&s_frequency);
    if (s_use_qpc) {
        LARGE_INTEGER now;
        QueryPerformanceCounter(&now);
        return (double)((1000LL * now.QuadPart) / s_frequency.QuadPart);
    } else {
        return GetTickCount();
    }
#else
#error implement get millis
#endif
}
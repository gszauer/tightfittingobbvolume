#ifndef _H_CAMERA_
#define _H_CAMERA_

#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include <vector>

class Camera {
public: // For testing, really!
    glm::mat4   view;   
    glm::mat4   projection;
public:
    void SetView(const glm::mat4& v);
    void SetProjection(const glm::mat4& p);
    
    std::vector<glm::vec3> GetWorldSpaceCorners();
    glm::mat4 GetView();
    glm::mat4 GetProjection();
    
    void RenderFixedFunction();
};

#endif
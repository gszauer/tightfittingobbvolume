#include "Obb.h"
#include "Plane.h"
#include "Platform.h"

OBB::OBB(glm::vec3 lightPosition, glm::vec3 lightNormal, std::vector<glm::vec3> points) {
    // 0) Variables
    glm::vec3 furthest = points[0];
    
    // 1) Find furthest point from light
    for (int i = 0, isize = int(points.size()); i < isize; ++i) {
        if (glm::distance2(lightPosition, points[i]) > glm::distance2(lightPosition, furthest)) {
            furthest = points[i];
        }
    }
    
    // 2) Project that vector onto the light normal to get the center point
    glm::vec3 lightToFurthest = furthest - lightPosition;
    glm::vec3 projected = glm::dot(lightToFurthest, lightNormal) * lightNormal;
    
    /*!*/ position = glm::lerp(lightPosition, lightPosition + projected, 0.5f);
    
    // 3) Create forward plane around the center point
    /*!*/ zAxis = glm::normalize(lightNormal);
    Plane forwardPlane(position, zAxis);
    
    // 4) Find right plane based on furthest point of projected frustum onto forward plane
    std::vector<glm::vec3> pointsOnForwardPlane = forwardPlane.ProjectPoints(points);
    furthest = pointsOnForwardPlane[0];
    for (int i = 0, isize = int(pointsOnForwardPlane.size()); i < isize; ++i) {
        if (glm::distance2(position, pointsOnForwardPlane[i]) > glm::distance2(position, furthest)) {
            furthest = pointsOnForwardPlane[i];
        }
    }
    /*!*/ xAxis = glm::normalize(glm::vec3(furthest - position));
    
    // 5) The up plane can be derived with a dot product
    /*!*/yAxis = glm::normalize(glm::cross(zAxis, xAxis));
    
    // 6) Find the right side size (furthest still holds furthest available point)
    extents.x = glm::length(furthest - position);
    
    // 7 Find the up side size
    Plane rightPlane(position, xAxis);
    std::vector<glm::vec3> pointsOnRightPlane = rightPlane.ProjectPoints(points);
    furthest = pointsOnRightPlane[0];
    for (int i = 0, isize = int(pointsOnRightPlane.size()); i < isize; ++i) {
        if (glm::distance2(position, pointsOnRightPlane[i]) > glm::distance2(position, furthest)) {
            furthest = pointsOnRightPlane[i];
        }
    }
    extents.y = glm::length(furthest - position);
    
    // 7) Find the forward size
    Plane upPlane(position, yAxis);
    std::vector<glm::vec3> pointsOnUpPlane = upPlane.ProjectPoints(points);
    furthest = pointsOnUpPlane[0];
    for (int i = 0, isize = int(pointsOnUpPlane.size()); i < isize; ++i) {
        if (glm::distance2(position, pointsOnUpPlane[i]) > glm::distance2(position, furthest)) {
            furthest = pointsOnUpPlane[i];
        }
    }
    extents.z = glm::length(furthest - position);
    
    extents.x = fabsf(extents.x);
    extents.y = fabsf(extents.y);
    extents.z = fabsf(extents.z);
}


void OBB::ToFrusutm(glm::mat4& outModelView, glm::mat4& outProjection) {
    for (int i = 0; i < 3; ++i)
        outModelView[0][i] = xAxis[i];
    for (int i = 0; i < 3; ++i)
        outModelView[1][i] = yAxis[i];
    for (int i = 0; i < 3; ++i)
        outModelView[2][i] = zAxis[i];
    for (int i = 0; i < 3; ++i)
        outModelView[3][i] = position[i];
    
    outModelView = glm::inverse(outModelView);
    
    outProjection = glm::ortho(-extents.x, extents.x, -extents.y, extents.y, -extents.z, extents.z);
}

void OBB::RenderFixedFunction() {
    glPushMatrix();
    glm::mat4 translation;
    for (int i = 0; i < 3; ++i)
        translation[0][i] = xAxis[i];
    for (int i = 0; i < 3; ++i)
        translation[1][i] = yAxis[i];
    for (int i = 0; i < 3; ++i)
        translation[2][i] = zAxis[i];
    for (int i = 0; i < 3; ++i)
        translation[3][i] = position[i];
    glMultMatrixf(glm::value_ptr(translation));
    
    std::vector<glm::vec3> points;
    float x = extents.x;
    float y = extents.y;
    float z = extents.z;
    points.push_back(glm::vec3(-x, -y, -z));
    points.push_back(glm::vec3(-x, +y, -z));
    points.push_back(glm::vec3(-x, +y, +z));
    points.push_back(glm::vec3(-x, -y, +z));
    points.push_back(glm::vec3(+x, -y, -z));
    points.push_back(glm::vec3(+x, -y, +z));
    points.push_back(glm::vec3(+x, +y, +z));
    points.push_back(glm::vec3(+x, +y, -z));
    
    unsigned int render[] = {0,1,2,0,3,2,4,5,6,6,7,4,0,4,5,5,3,0,1,2,6,6,7,1,3,5,6,6,2,3,0,4,7,7,1,0};
    
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glDisable(GL_CULL_FACE);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &points[0].x);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, render);
    glDisableClientState(GL_VERTEX_ARRAY);
    glEnable(GL_CULL_FACE);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    
    glBegin(GL_LINES);
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f(1.0f, 0.0f, 0.0f);
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f(0.0f, 1.0f, 0.0f);
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 0.0f, 1.0f);
    glEnd();
    
    glPopMatrix();
}
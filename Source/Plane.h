#ifndef _H_PLANE_
#define _H_PLANE_

#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include <vector>

struct Plane {
    glm::vec3 normal;
    float distance;
    
    Plane(const glm::vec3& point, const glm::vec3& norm);
    
    std::vector<glm::vec3> ProjectPoints(const std::vector<glm::vec3>& points);
    glm::vec3 ProjectPoint(const glm::vec3& point);
};

#endif
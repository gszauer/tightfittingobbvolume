#include "Application.h"
#include "ObjLoader.h"
#include "Camera.h"
#include "Platform.h"
#include "Obb.h"
#include "Plane.h"
#include <iostream>

#define DONT_EXECUTE    0
#define DONT_UPDATE     0

Camera viewCamera;
Camera freeCamera;
Camera shadowCamera;

glm::vec2 cameraAngle;
glm::vec3 lightPosition;
glm::vec3 lightNormal;
extern float updateSpeed;

Obj* sphere = 0;
OBB* obb = 0;

glm::vec3 GetCameraPosition(float distance) {
    static const float PI_180 = (M_PI/180.0f);
    float camX = distance * -sinf(cameraAngle.x * PI_180) * cosf((cameraAngle.y)*PI_180);
    float camY = distance * -sinf((cameraAngle.y)*PI_180);
    float camZ = -distance * cosf((cameraAngle.x)*PI_180) * cosf((cameraAngle.y)*PI_180);
    return glm::vec3(camX, camY, camZ);
}

bool Initialize(int w, int h) {
    glm::vec3 origin = glm::vec3(0.0f);
    glm::vec3 one = glm::vec3(1.0f);
    
    glm::vec3 originOne = origin - one;
    glm::vec3 oneOrigin = one - origin;
    
    std::cout << "(0,0,0) - (1,1,1): " << originOne.x << ", " << originOne.y << ", " << originOne.z << "\n";
    std::cout << "(1,1,1) - (0,0,0): " << oneOrigin.x << ", " << oneOrigin.y << ", " << oneOrigin.z << "\n";
    std::cout << "destination - source\n\n";
    
    glm::vec3 forward(0.0f, 0.0f, 1.0f);
    glm::vec3 right(1.0f, 0.0f, 0.0f);
    
    glm::vec3 forwardRight = glm::cross(forward, right);
    glm::vec3 rightForward = glm::cross(right, forward);
    
    std::cout << "forward X right: " << forwardRight.x << ", " << forwardRight.y << ", " << forwardRight.z << "\n";
    std::cout << "right X forward: " << rightForward.x << ", " << rightForward.y << ", " << rightForward.z << "\n";
    std::cout << "forward X right = up\n\n";
    
    glm::mat4 matrix;
    float* values = (float*)&matrix[0][0];
    for (int i = 0; i < 16; ++i)
        values[i] = i;
    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j)
            std::cout << i << ", " << j << ": " << matrix[i][j] << "\n";
    std::cout << "\n";
    
    
#if DONT_EXECUTE
    return false;
#endif
    
    viewCamera.SetProjection(glm::perspective(60.0f, 800.0f / 600.0f, 1.0f, 10.0f));
    viewCamera.SetView(glm::lookAt(glm::vec3(5.0f, 5.0f, 5.0f), glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
    
    lightPosition = glm::vec3(5.0f, 10.0f, 7.0f);
    lightNormal = -glm::normalize(lightPosition);
    
    freeCamera.SetProjection(glm::perspective(60.0f, float(w) / float(h), 0.01f, 1000.0f));
    freeCamera.SetView(glm::lookAt(glm::vec3(-50.0f, 7.0f, 0.0f), glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f)));
    
    sphere = new Obj("./Assets/Sphere.obj");
    
    if (sphere->GetError())
        return false;
    
    cameraAngle.x = 0.0f;
    cameraAngle.y = 15.0f;
    
    std::vector<glm::vec3> set = viewCamera.GetWorldSpaceCorners();
    set.push_back(lightPosition);
    obb = new OBB(lightPosition, lightNormal, set);
    obb->ToFrusutm(shadowCamera.view, shadowCamera.projection);
    
    
    return true;
}

bool Update(float dt) {
#if DONT_EXECUTE
    return false;
#endif
#if DONT_UPDATE
    return true;
#endif
    cameraAngle.x += 90.0f * dt * updateSpeed;
    while (cameraAngle.x > 360.0f)
        cameraAngle.x -= 360.0f;
    glm::vec3 newPos = GetCameraPosition(-50.0f);
    freeCamera.SetView(glm::lookAt(glm::vec3(newPos.x, newPos.y, newPos.z), glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

    return true;
}

void DrawPointPoly(const std::vector<glm::vec3>& points) {
    glDisable(GL_CULL_FACE);
    unsigned int render[] = {1,3,7,1,5,7,0,2,4,0,6,4,1,3,0,1,2,0,5,7,6,5,4,6,3,7,6,3,0,6,1,5,4,1,2,4};
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &points[0].x);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, render);
    glDisableClientState(GL_VERTEX_ARRAY);
    glEnable(GL_CULL_FACE);
}

void DrawSphere(const glm::vec3& point, float r, float g, float b) {
    glColor3f(r, g, b);
    glPushMatrix();
    glTranslatef(point.x, point.y, point.z);
    glScalef(0.25f, 0.25f, 0.25f);
    sphere->RenderFixedFunction();
    glPopMatrix();
}

void DrawAxis(float x, float y, float z) {
    glColor3f(x, y, z);
    glBegin(GL_LINES);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(x, y, z);
    glEnd();
}

void Render() {
#if DONT_EXECUTE
    return;
#endif
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMultMatrixf(glm::value_ptr(freeCamera.GetProjection()));
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMultMatrixf(glm::value_ptr(freeCamera.GetView()));
    
    // Render perspective camera
    glColor3f(1.0f, 1.0f, 0.0f);
    viewCamera.RenderFixedFunction();

    // Render Light
    glPushMatrix();
        glColor3f(1.0f, 1.0f, 1.0f);
        glPushMatrix();
        glTranslatef(lightPosition.x, lightPosition.y, lightPosition.z);
        glScalef(0.25f, 0.25f, 0.25f);
        sphere->RenderFixedFunction();
        glPopMatrix();
        glBegin(GL_LINES);
        glVertex3f(lightPosition.x, lightPosition.y, lightPosition.z);
        glVertex3f(lightPosition.x + lightNormal.x, lightPosition.y + lightNormal.y, lightPosition.z + lightNormal.z);
        glEnd();
    glPopMatrix();
    
    //glColor3f(1.0f, 0.0f, 1.0f);
    //obb->RenderFixedFunction();
    
    glColor3f(0.0f, 1.0f, 1.0f);
    shadowCamera.RenderFixedFunction();
    
    DrawAxis(1.0f, 0.0f, 0.0f);
    DrawAxis(0.0f, 1.0f, 0.0f);
    DrawAxis(0.0f, 0.0f, 1.0f);
}

void Shutdown() {
    if (sphere != 0)
        delete sphere;
    if (obb != 0)
        delete obb;
    
    obb = 0;
    sphere = 0;
}

void Resize(int w, int h) {
    freeCamera.SetProjection(glm::perspective(60.0f, float(w) / float(h), 0.01f, 1000.0f));
}
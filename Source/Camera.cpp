#include "Camera.h"
#include "Platform.h"

void Camera::SetView(const glm::mat4& v) {
    view = v;
}

void Camera::SetProjection(const glm::mat4& p) {
    projection = p;
}

glm::mat4 Camera::GetView() {
    return view;
}

glm::mat4 Camera::GetProjection() {
    return projection;
}

std::vector<glm::vec3> Camera::GetWorldSpaceCorners() {
    // http://www.gamedev.net/topic/651106-world-space-camera-frustum/
    glm::mat4 viewProjMatrix = projection * view;
    glm::mat4 inverseProjectionMatrix = glm::inverse(viewProjMatrix);
    
    std::vector<glm::vec3> corners; corners.reserve(8);
    for (unsigned x = 0; x < 2; x++) {
        for (unsigned y = 0; y < 2; y++){
            for (unsigned z = 0; z < 2; z++) { // I might mess up the order of the frustrum corners here. But the position should be correct
                glm::vec4 projClipSpacePosition(x*2.0f-1.0f, y*2.0f-1.0f, z*2.0f-1.0f, 1.0f);
                glm::vec4 projWorldSpacePosition = inverseProjectionMatrix * projClipSpacePosition;
                corners.push_back(glm::vec3(projWorldSpacePosition.x / projWorldSpacePosition.w,
                                            projWorldSpacePosition.y / projWorldSpacePosition.w,
                                            projWorldSpacePosition.z / projWorldSpacePosition.w
                                ));
            }
        }
    }
    
    return corners;
}

// This works! The above version is just cleaner!
/*std::vector<glm::vec3> Camera::GetWorldSpaceCorners() {
    // http://gamedev.stackexchange.com/questions/29999/how-do-i-create-a-bounding-frustum-from-a-view-projection-matrix
    
    std::vector<glm::vec4> frustum; frustum.resize(8);
    
    frustum[0] = glm::vec4(-1.0f,  1.0f,  1.0f, 1.0f);
    frustum[1] = glm::vec4( 1.0f,  1.0f,  1.0f, 1.0f);
    frustum[2] = glm::vec4(-1.0f, -1.0f,  1.0f, 1.0f);
    frustum[3] = glm::vec4( 1.0f, -1.0f,  1.0f, 1.0f);
    frustum[4] = glm::vec4(-1.0f,  1.0f, -1.0f, 1.0f);
    frustum[5] = glm::vec4( 1.0f,  1.0f, -1.0f, 1.0f);
    frustum[6] = glm::vec4(-1.0f, -1.0f, -1.0f, 1.0f);
    frustum[7] = glm::vec4( 1.0f, -1.0f, -1.0f, 1.0f);
    
    for (int i = 0; i < 8; ++i) {
        glm::vec4 projectionSpace = glm::inverse(projection) * frustum[i];
        glm::vec4 viewSpace = projectionSpace * (1.0f / projectionSpace.w);
        frustum[i]= glm::inverse(view) * viewSpace;
    }
    
    std::vector<glm::vec3> result; result.resize(8);
    for (int i = 0; i < 8; ++i)
        result[i] = glm::vec3(frustum[i].x, frustum[i].y, frustum[i].z);
    return result;
}*/

void Camera::RenderFixedFunction() {
    std::vector<glm::vec3> corners = GetWorldSpaceCorners();
    unsigned int render[] = {1,3,1,5,7,3,7,5,0,2,0,4,6,2,6,4,6,7,2,3,0,1,4,5};
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, &corners[0].x);
    glDrawElements(GL_LINES, 24, GL_UNSIGNED_INT, render);
    glDisableClientState(GL_VERTEX_ARRAY);
    /*glBegin(GL_LINES);
    for (int i = 0, isize = corners.size(); i < isize; ++i) {
        for (int j = 0, jsize = corners.size(); j < jsize; ++j) {
            glVertex3f(corners[i].x, corners[i].y, corners[i].z);
            glVertex3f(corners[j].x, corners[j].y, corners[j].z);
        }
    }
    glEnd();*/
}